let post = [];
let count = 1;


// innerHTML - refers to the literal HTML markup
// value property - content of element

// add post data

document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	// prevents the page from loading
	event.preventDefault()

	post.push({
		id : count,
		title : document.querySelector('#txt-title').value,
		body : document.querySelector('#txt-body').value
	})

	count++;


	showPosts(post)
	alert('Successfully added.')
})


const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div

		`;
	})

	document.querySelector('#div-post-entries').innerHTML=postEntries;
}




// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}



// update post

document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
	event.preventDefault()

	for(let i=0; i < post.length; i++){
		if(post[i].id.toString() === document.querySelector('#txt-edit-id').value){
			post[i].title = document.querySelector('#txt-edit-title').value
			post[i].body = document.querySelector('#txt-edit-body').value

			showPosts(post)
			alert('Successfully updated.')

			break
		}
	}
})




/* MINI ACTIVITY


function editPost(postId) {
	const pamagat = post[postId-1].title
	const desc = post[postId-1].body


	document.querySelector('#txt-edit-title').innerHTML = pamagat;
	document.querySelector('#txt-edit-body').innerHTML = desc;



	document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
		// prevents the page from loading
		event.preventDefault()

		post[postId-1].title = document.querySelector('#txt-edit-title').value
		post[postId-1].body = document.querySelector('#txt-edit-body').value

		showPosts(post)
		alert('Successfully updated.')
	})
}


*/